<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipoEstudio;
use App\Models\Ingreso;
use App\Models\Medico;
use App\Models\Paciente;
use App\Models\DocIngreso;
use Illuminate\Support\Facades\Storage;

class IngresoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ingreso.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $medicos = Medico::pluck('nombre', 'id');
        $tipoEstudios = TipoEstudio::pluck('nombre', 'id');
        $paciente = Paciente::find($id);
        return view('ingreso.create', compact('paciente', 'medicos', 'tipoEstudios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq, $id)
    {
        $ingreso = new Ingreso;
        $ingreso->paciente_id = $id;
        $ingreso->medico_id = $rq->medico_id;
        $ingreso->tipo_estudio_id = $rq->tipo_estudio_id;
        $ingreso->observaciones = $rq->observaciones;
        $ingreso->save();

        return redirect("/paciente/$id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id, $ingresoId)
    {
        $file = $rq->file('file');
        $imageName = Functions::uuidV4(); 
        $filePath = "$id/$ingresoId/$imageName.{$file->getClientOriginalExtension()}";
        Storage::disk('s3')->put($filePath, file_get_contents($file));

        $imgIngreso = new DocIngreso();
        $imgIngreso->ingreso_id = $id;
        $imgIngreso->imagen = $filePath;
        $imgIngreso->save();

        return $filePath; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
