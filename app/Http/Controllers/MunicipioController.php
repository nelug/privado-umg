<?php

namespace App\Http\Controllers;

use App\Models\Departamento;
use App\Models\Municipio;
use Illuminate\Http\Request;

class MunicipioController extends Controller
{
  /**
   * Undocumented function
   *
   * @param [type] $id
   * @return void
   */
  public function index($id)
  {
    return Municipio::whereDepartamentoId($id)->orderBy('nombre')->get();
  }
 
}
