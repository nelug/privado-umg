<?php

namespace App\Http\Controllers;
use App\Models\TipoEstudio;
use App\Models\Ingreso;
use App\Models\Medico;
use App\Models\Paciente;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $countMedicos = Medico::count();
        $countPacientes = Paciente::count();
        $countIngresos = Ingreso::count();
        $countEstudios = TipoEstudio::count();

        return view('dashboard', compact('countMedicos', 'countPacientes', 'countIngresos', 'countEstudios'));
    }
}
