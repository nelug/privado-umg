<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Medico;

class MedicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medicos = Medico::paginate(5);
        return view('medico.index', compact('medicos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medico.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $medico = new Medico([
            'nombre' => $rq->get('nombre'),
            'correo' => $rq->get('correo'),
            'telefono' => $rq->get('telefono'),
            'direccion_hospital' => $rq->get('direccion_hospital'),
            'direccion_clinica' => $rq->get('direccion_clinica'),
            'estado' => true,
            'user_id' => 1,
        ]);

        $medico->save();
        return redirect('/medico');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $medico = Medico::find($id);
        return view('medico.edit', compact('medico'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $medico= Medico::find($id);
        $medico->nombre = $rq->get('nombre');
        $medico->telefono = $rq->get('telefono');
        $medico->correo = $rq->get('correo');
        $medico->direccion_hospital = $rq->get('direccion_hospital');
        $medico->direccion_clinica = $rq->get('direccion_clinica');
        $medico->save();
        
        return redirect('/medico');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
