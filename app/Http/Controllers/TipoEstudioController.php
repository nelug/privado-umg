<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipoEstudio;

class TipoEstudioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estudios = TipoEstudio::paginate(5);
        return view('tipo-estudio.index',compact('estudios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipo-estudio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $estudio = new TipoEstudio([
            'nombre' => $rq->get('nombre'),
            'user_id' => 1,
        ]);

        $estudio->save();
        return redirect('/tipo-estudio');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estudio = TipoEstudio::find($id);
        return view('tipo-estudio.edit', compact('estudio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $estudio= TipoEstudio::find($id);
        $estudio->nombre = $rq->get('nombre');
        $estudio->save();
        
        return redirect('/tipo-estudio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
