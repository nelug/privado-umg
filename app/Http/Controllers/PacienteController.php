<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paciente;
use App\Models\Municipio;
use App\Models\Departamento;
use App\Models\Genero;
use App\Models\Ingreso;

class PacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $rq)
    {
        if ($rq->ac == true) {
            $buscar = strtolower($rq->get("term"));
            $queryLike = Functions::queryLike($buscar, "LOWER(COALESCE(dpi,'') || nombre)");
            return Paciente::with('municipio')->select("id", "nombre as value")->whereRaw($queryLike)->get();
        }

        $buscar = strtolower($rq->get("buscar"));
        $queryLike = Functions::queryLike($buscar, "LOWER(COALESCE(dpi,'') || nombre)");

        $pacientes = Paciente::whereRaw($queryLike)->paginate(10);
        return view("paciente.index", compact('pacientes', 'buscar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos = Departamento::orderBy('nombre')->pluck('nombre', 'id');
        $municipios = Municipio::whereDepartamentoId(16)->orderBy('nombre')->pluck('nombre', 'id');
        $generos = Genero::all();
        return view('paciente.create', compact('departamentos','municipios','generos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $paciente = new Paciente([
            'nombre' => $rq->get('nombre'),
            'correo' => $rq->get('correo'),
            'telefono' => $rq->get('telefono'),
            'genero_id' => $rq->get('genero_id'),
            'municipio_id' => $rq->get('municipio_id'),
            'dpi' => $rq->get('dpi'),
            'direccion' => $rq->get('direccion'),
            'fecha_nacimiento' => $rq->get('fecha_nacimiento'),
            'user_id' => 1,
        ]);

        $paciente->save();
        return redirect('/paciente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paciente = Paciente::find($id);
        $ingresos = Ingreso::wherePacienteId($id)->paginate(5); 
        return view('paciente.show', compact('id', 'ingresos', 'paciente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paciente = Paciente::find($id);
        $departamentos = Departamento::orderBy('nombre')->pluck('nombre', 'id');
        $municipios = Municipio::whereDepartamentoId($paciente->municipio->departamento_id)->orderBy('nombre')->pluck('nombre', 'id');
        $generos = Genero::all();
        return view('paciente.edit', compact('paciente','departamentos','municipios','generos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $paciente= Paciente::find($id);
        $paciente->nombre = $rq->get('nombre');
        $paciente->telefono = $rq->get('telefono');
        $paciente->correo = $rq->get('correo');
        $paciente->genero_id = $rq->get('genero_id');
        $paciente->municipio_id = $rq->get('municipio_id');
        $paciente->dpi = $rq->get('dpi'); 
        $paciente->direccion = $rq->get('direccion');
        $paciente->fecha_nacimiento = $rq->get('fecha_nacimiento');
        $paciente->save();
        
        return redirect('/paciente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
