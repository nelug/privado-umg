<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    use HasFactory;
    protected $guarded = array('id'); 

    public function medico()
    {
        return $this->belongsTo('App\Models\Medico');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\Estado');
    }

    public function tipoEstudio()
    {
        return $this->belongsTo('App\Models\TipoEstudio');
    }

    public function docs()
    {
        return $this->hasMany('App\Models\DocIngreso')->select('imagen');
    }
}
