<?php

namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{ 
    protected $guarded = array('id'); 

    public function genero()
    {
        return $this->belongsTo(Genero::class, 'genero_id');    
    }

    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'municipio_id');    
    }
}