<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenerosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('generos')->insert([
            ['nombre' => "Masculino"],
            ['nombre' => "Femenino"],
            ['nombre' => "S/R"],
            ['nombre' => "Animal"],
            ['nombre' => "Sin Informacion"]
        ]);

        DB::table("generos")->update(["created_at" => now(), "updated_at" => now()]);
    }
}
