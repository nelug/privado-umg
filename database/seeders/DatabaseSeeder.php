<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([UsersTableSeeder::class]);
        $this->call([DepartamentosSeeder::class]);
        $this->call([MunicipiosSeeder::class]);
        $this->call([GenerosSeeder::class]);
        $this->call([TipoEstudioSeeder::class]);
        $this->call([EstadosSeeder::class]);
    }
}
