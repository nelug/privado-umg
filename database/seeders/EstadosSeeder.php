<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados')->insert([
            ['nombre' => "En Proceso" ],
            ['nombre' => "Finalizado" ],
            ['nombre' => "Anulado " ]
        ]);

        DB::table("estados")->update(["created_at" => now(), "updated_at" => now()]);
    }
}
