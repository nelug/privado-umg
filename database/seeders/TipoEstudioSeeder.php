<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoEstudioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_estudios')->insert([
            ['nombre' => "RayosX" ],
            ['nombre' => "USG" ],
            ['nombre' => "Doppler" ],
            ['nombre' => "Mamografia" ],
            ['nombre' => "General" ]
        ]);

        DB::table("tipo_estudios")->update(["created_at" => now(), "updated_at" => now()]);
    }
}
