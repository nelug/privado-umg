<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departamentos')->insert([
            ["nombre" => "GUATEMALA"],
            ["nombre" => "EL PROGRESO"],
            ["nombre" => "SACATEPEQUEZ"],
            ["nombre" => "CHIMALTENANGO"],
            ["nombre" => "ESCUINTLA"],
            ["nombre" => "SANTA ROSA"],
            ["nombre" => "SOLOLA"],
            ["nombre" => "TOTONICAPAN"],
            ["nombre" => "QUETZALTENANGO"],
            ["nombre" => "SUCHITEPEQUEZ"],
            ["nombre" => "RETALHULEU"],
            ["nombre" => "SAN MARCOS"],
            ["nombre" => "HUEHUETENANGO"],
            ["nombre" => "EL QUICHE"],
            ["nombre" => "BAJA VERAPAZ"],
            ["nombre" => "ALTA VERAPAZ"],
            ["nombre" => "EL PETEN"],
            ["nombre" => "IZABAL"],
            ["nombre" => "ZACAPA"],
            ["nombre" => "CHIQUIMULA"],
            ["nombre" => "JALAPA"],
            ["nombre" => "JUTIAPA"]
        ]);

        DB::table("departamentos")->update(["created_at" => now(), "updated_at" => now()]);
    }
}
