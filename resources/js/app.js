/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import formatStringByPattern from 'format-string-by-pattern';
window.Vue = require('vue').default;


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        dpi: "",
        docs: [],
        pacienteId: 0,
        ingresoId: 0
    },
    filters: {
        capitalize: function (value) {
            if (!value) return ''
            value = value.toString()
            return value.charAt(0).toUpperCase() + value.slice(1)
        },
        onlyNumbers(value) {
            if (!value) return '';
            const onlyNumbers = value.replace(/[^\d]/g, '');
            return formatStringByPattern('9999 99999 9999', onlyNumbers);
        },
        fileType(value) {
            let tipo = 'far fa-file fa-2x';
            if (value.includes(".jpg") || value.includes(".jpeg") || value.includes(".png") || value.includes(".svg") || value.includes(".gif")) {
                tipo = 'fas fa-file-image fa-2x text-info';
            }
            if (value.includes(".doc") || value.includes(".docdx")) {
                tipo = 'far fa-file-word fa-2x text-primary';
            }
            if (value.includes(".ppt") || value.includes(".pptx")) {
                tipo = 'far fa-file-powerpoint fa-2x text-danger';
            }
            if (value.includes(".xls") || value.includes(".xlsx")) {
                tipo = 'far fa-file-excel fa-2x text-success';
            }
            if (value.includes(".pdf")) {
                tipo = 'far fa-file-pdf fa-2x text-warning';
            }

            return tipo;
        }
    },
    methods: {
        formatOnlyNumbers: () => {
            const onlyNumbers = app.dpi.replace(/[^\d]/g, '');
            app.dpi = formatStringByPattern('9999 99999 9999', onlyNumbers);
        },

        habilitarEnvio: (dpi) => {
            let envio = true;
            if (dpi != undefined && dpi != '') {
                if (dpi.length == 15) {
                    envio = false;
                }
            }
            return envio;
        },

        viewDocs: (docs) => {
            console.log(docs);
            app.docs = docs;
        },
        mostrarModal: (id, ingreso) => {
            $("#docsModal").modal('show');
            var previewNode = document.querySelector("#template");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzone = new Dropzone(document.body, { 
                url: `/paciente/${id}/ingreso/${ingreso}/upload`, 
                thumbnailWidth: 80,
                thumbnailHeight: 80,
                parallelUploads: 20,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                previewTemplate: previewTemplate,
                autoQueue: false, 
                previewsContainer: "#previews",  
                clickable: ".fileinput-button"  
            }); 

            document.querySelector("#actions .start").onclick = function () {
                myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
            };
            document.querySelector("#actions .cancel").onclick = function () {
                myDropzone.removeAllFiles(true);
            }; 

        },
        addedfile: (file) => {
            file.previewElement.querySelector(".start").onclick = function () {
                myDropzone.enqueueFile(file);
            };
        },

        totaluploadprogress: (progress) => {
            document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
        },

        sending: (file) => {
            document.querySelector("#total-progress").style.opacity = "1";
            file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
        },

        queuecomplete: (progress) => {
            document.querySelector("#total-progress").style.opacity = "0";
        },

        visualizarUrl: (url) => {
            console.log(url);
            let office = false;

            if (url.includes(".doc") || url.includes(".docdx") ||
                url.includes(".ppt") || url.includes(".pptx") ||
                url.includes(".xls") || url.includes(".xlsx")) {
                office = true;
            }

            url = "https://privado-umg.s3.amazonaws.com/"+url;
            
            if (office) {
                url = "https://view.officeapps.live.com/op/embed.aspx?src=" + url;
            }
            $("#preview-resource").attr('src', url);

            $("#previewModal").modal('show');
        }
    }
});

