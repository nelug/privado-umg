@extends('layouts.app', ['title' => __('User Profile')])

@section('content')
    @include('layouts.headers.empty', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('This is your profile page. You can see the progress you\'ve made with your work and manage your projects or assigned tasks'),
        'class' => 'col-lg-7'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-3"></div>
            <div class="col-xl-6 ">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="mb-0">{{ __('Registrar Estudio') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('ingreso.store', $paciente->id) }}" autocomplete="off">
                            @csrf
                            <div class="pl-lg-4">

                                <div class="form-group">
                                    <label class="form-control-label" for="input-name">{{ __('Paciente') }}</label>
                                    <input type="text" class="form-control" value="{{$paciente->nombre}}" placeholder="Paciente" disabled="true" >
                                </div>


                                <div class="form-group{{ $errors->has('tipo_estudio_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Tipo Estudio') }}</label>
                                    {{ Form::select('tipo_estudio_id', $tipoEstudios, 1, ['class' => 'form-control']) }}
                                </div> 

                                <div class="form-group{{ $errors->has('medico_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Medico') }}</label>
                                    {{ Form::select('medico_id', $medicos, 1, ['class' => 'form-control']) }}
                                </div> 
                                <div class="form-group{{ $errors->has('observaciones') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Observacion') }}</label>
                                    <textarea type="text" name="observaciones" class="form-control" placeholder="Observacion" > 

                                    </textarea>
                                </div> 
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xl-3"></div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection
