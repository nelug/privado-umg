@extends('layouts.app')

@push('css') 

@endpush

@section('content')
    @include('layouts.headers.empty')

    <div class="container-fluid mt--8">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Ingresos</h3>
                            </div> 
                        </div>
                    </div>
                    <div class="table-responsive">
                        
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js') 

@endpush
