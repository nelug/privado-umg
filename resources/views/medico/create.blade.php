@extends('layouts.app', ['title' => __('User Profile')])

@section('content')
    @include('layouts.headers.empty', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('This is your profile page. You can see the progress you\'ve made with your work and manage your projects or assigned tasks'),
        'class' => 'col-lg-7'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-3"></div>
            <div class="col-xl-6 ">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="mb-0">{{ __('Registrar Medico') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('medico.store') }}" autocomplete="off">
                            @csrf
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('medico') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Nombre') }}</label>
                                    <input type="text" name="nombre"  class="form-control" placeholder="nombre" required >
                                </div>
                                <div class="form-group{{ $errors->has('medico') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Correo') }}</label>
                                    <input type="text" name="correo" class="form-control" placeholder="correo" >
                                </div>
                                <div class="form-group{{ $errors->has('medico') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Telefono') }}</label>
                                    <input type="text" name="telefono"  class="form-control" placeholder="telefono" >
                                </div>
                                <div class="form-group{{ $errors->has('medico') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Direccion Clinica') }}</label>
                                    <input type="text" name="direccion_clinica" class="form-control" placeholder="direccion clinica" >
                                </div>
                                <div class="form-group{{ $errors->has('medico') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Direccion Hospital') }}</label>
                                    <input type="text" name="direccion_hospital" class="form-control" placeholder="direccion hospital" >
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xl-3"></div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection
