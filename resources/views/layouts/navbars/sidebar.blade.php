<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" 
        aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{ route('home') }}">
            <img src="{{ asset('argon') }}/img/brand/blue.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle" style="background: #6071e4;">
                            <i class="fas fa-user-circle" style="font-size: 30px; color: white"></i>
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Bienvenido!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('Mi Perfil') }}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('argon') }}/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="{{ __('Search') }}" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">
                        <i class="ni ni-tv-2 text-primary"></i> {{ __('Escritorio') }}
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" href="#navbar-examples" data-toggle="collapse" role="button" aria-controls="navbar-examples">
                        <i class="ni ni-circle-08" style="color: #f4645f;"></i>
                        <span class="nav-link-text" style="color: #f4645f;">{{ __('Usuarios') }}</span>
                    </a>

                    <div class="collapse " id="navbar-examples">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('profile.edit') }}">
                                    {{ __('Perfil de usuario') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('user.index') }}">
                                    {{ __('Manejo de usuarios') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                 --}}

                <li class="nav-item">
                    <a class="nav-link " href="#navbar-pacientes" data-toggle="collapse" role="button" aria-controls="navbar-examples">
                        <i class="fab fa-search" style="color: #2dce89"></i>
                        <span class="nav-link-text" style="color: #2dce89">{{ __('Pacientes') }}</span>
                    </a>

                    <div class="collapse " id="navbar-pacientes">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="/paciente">
                                    Ver Pacientes
                                </a>
                            </li> 
                            <li class="nav-item">
                                <a class="nav-link" href="/paciente/create">
                                    Crear Pacientes
                                </a>
                            </li> 
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link " href="#navbar-medicos" data-toggle="collapse" role="button" aria-controls="navbar-examples">
                        <i class="fab fa-search" style="color: #11cdef"></i>
                        <span class="nav-link-text" style="color: #11cdef">{{ __('Medicos') }}</span>
                    </a>

                    <div class="collapse " id="navbar-medicos">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="/medico">
                                    Ver Medicos
                                </a>
                            </li> 
                            <li class="nav-item">
                                <a class="nav-link" href="/medico/create">
                                    Crear Medicos
                                </a>
                            </li> 
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link " href="#navbar-tipo-estudios" data-toggle="collapse" role="button" aria-controls="navbar-examples">
                        <i class="fab fa-search" style="color: #5e72e4"></i>
                        <span class="nav-link-text" style="color: #5e72e4">{{ __('Tipos de Estudio') }}</span>
                    </a>

                    <div class="collapse " id="navbar-tipo-estudios">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="/tipo-estudio">
                                    Ver Tipos de Estudio
                                </a>
                            </li> 
                            <li class="nav-item">
                                <a class="nav-link" href="/tipo-estudio/create">
                                    Crear Tipo de Estudio
                                </a>
                            </li> 
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
