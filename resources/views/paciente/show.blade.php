@extends('layouts.app')


@push('css')
    <style>
        .paginacion svg {
            width: 25px;
        }

        .paginacion>nav>div>a {
            display: none;
        }

        .paginacion>nav>div>span {
            display: none;
        }

    </style>
@endpush

@section('content')
    @include('layouts.headers.empty')

    <div class="container-fluid mt--8">
        <div class="row">
            <div class="col ">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Listado de Estudios</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="/paciente/{{ $id }}/ingreso/create" class="btn btn-sm btn-primary">Crear
                                    Ingreso</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 row px-5 mb-5">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <th class="px-3">Nombre:</th>
                                        <td>{{ $paciente->nombre }}</td>
                                    </tr>
                                    <tr>
                                        <th class="px-3">DPI:</th>
                                        <td>{{ $paciente->dpi }}</td>
                                    </tr>
                                    <tr>
                                        <th class="px-3">Genero:</th>
                                        <td>{{ $paciente->genero->nombre }}</td>
                                    </tr>
                                </table>

                            </div>
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <th class="px-3">Departamento:</th>
                                        <td>{{ $paciente->municipio->departamento->nombre }}</td>
                                    </tr>
                                    <tr>
                                        <th class="px-3">Municipio:</th>
                                        <td>{{ $paciente->municipio->nombre }}</td>
                                    </tr>
                                    <tr>
                                        <th class="px-3">Direccion:</th>
                                        <td> {{ $paciente->direccion }} </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="table-responsive col-md-9">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" class="sort" data-sort="name">ID</th>
                                        <th scope="col" class="sort" data-sort="name">Observacion</th>
                                        <th scope="col" class="sort" data-sort="budget">Medico</th>
                                        <th scope="col" class="sort" data-sort="budget">Estudio</th>
                                        <th scope="col" class="sort" data-sort="status">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody class="list">
                                    @foreach ($ingresos as $p)
                                        <tr>
                                            <td> {{ $p->id }} </td>
                                            <td> {{ $p->observaciones }} </td>
                                            <td> {{ $p->medico->nombre }} </td>
                                            <td> {{ $p->tipoEstudio->nombre }} </td>
                                            <td>
                                                <a href="javascript:void(0)" class="mx-1"
                                                    v-on:click="viewDocs({{ $p->docs }})">
                                                    <i class="fas fa-eye" title="Ver"></i>
                                                </a>

                                                <a href="javascript:void(0)"
                                                    v-on:click="mostrarModal({{ $id }} ,{{ $p->id }})"
                                                    class="mx-1">
                                                    <i class="fas fa-file-upload"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                        <div class="col-md-3 table-responsive">

                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Archivo</th>
                                        <th scope="col">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr v-for="doc in docs">
                                        <th scope="row">
                                            <i v-bind:class="doc.imagen | fileType"></i>
                                        </th>
                                        <td>
                                            <a href="javascript:void(0)">
                                                <i class="fas fa-external-link-alt mx-1" v-on:click="visualizarUrl(doc.imagen)"></i>
                                            </a>
                                            <a class=" mx-1" :href="'https://privado-umg.s3.amazonaws.com/' + doc.imagen" target="_blank">
                                                <i class="fas fa-download"></i>
                                            </a>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- Card footer -->
                    <div class="card-footer py-4 paginacion">
                        {{ $ingresos->links() }}
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
        <div class="modal fade" id="docsModal" tabindex="-1" role="dialog" aria-labelledby="docsModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="docsModalLabel">Subir Archivo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="actions" class="row">

                            <div class="col-lg-7 d-flex">
                                <!-- The fileinput-button span is used to style the file input field as button -->
                                <span class="btn btn-success btn-sm fileinput-button dz-clickable">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Agregar Archivo...</span>
                                </span>
                                <button type="submit" class="btn  btn-sm btn-primary start">
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>Subir Todos</span>
                                </button>
                                <button type="reset" class="btn  btn-sm btn-warning cancel">
                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                    <span>Cancelar Todos</span>
                                </button>
                            </div>

                            <div class="col-lg-5">
                                <span class="fileupload-process">
                                    <div id="total-progress" class="progress progress-striped active" role="progressbar"
                                        aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="opacity: 0;">
                                        <div class="progress-bar progress-bar-success" style="width: 100%;"
                                            data-dz-uploadprogress=""></div>
                                    </div>
                                </span>
                            </div>

                        </div>
                        <div class="table table-striped" class="files" id="previews">

                            <div id="template" class="file-row d-flex justify-content-between">
                                <div>
                                    <span class="preview"><img data-dz-thumbnail /></span>
                                </div>
                                <div>
                                    <p class="name" data-dz-name></p>
                                    <strong class="error text-danger" data-dz-errormessage></strong>
                                </div>
                                <div>
                                    <p class="size" data-dz-size></p>
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0"
                                        aria-valuemax="100" aria-valuenow="0">
                                        <div class="progress-bar progress-bar-success" style="width:0%;"
                                            data-dz-uploadprogress></div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  btn-sm start">
                            <i class="glyphicon glyphicon-upload"></i>
                            <span>Subir</span>
                        </button>
                        <button data-dz-remove class="btn btn-warning btn-sm cancel" data-dismiss="modal" >
                            <i class="glyphicon glyphicon-ban-circle"></i>
                            <span>Cancelar</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="docsModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" style="max-width: 90% !important">
              <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe src="" id="preview-resource" frameborder="0" style="height: 80vh" width="100%"></iframe>
                </div>
              </div>
            </div>
          </div>
        
    </div>

@endsection

@push('js')
@endpush
