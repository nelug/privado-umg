@extends('layouts.app')


@push('css')
<style>
    .paginacion  svg {
        width: 25px;
    }
    
    .paginacion > nav > div > a {
        display: none;
    } 
    
    .paginacion > nav > div > span {
        display: none;
    }
</style>
@endpush
@section('content')
    @include('layouts.headers.empty')

    <div class="container-fluid mt--8">
        <div class="row">
            <div class="col ">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                          <div class="col-8">
                            <h3 class="mb-0">Listado de pacientes</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="/paciente/create" class="btn btn-sm btn-primary">Agregar paciente</a>
                        </div>
                        </div>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" class="sort" data-sort="name">ID</th>
                                    <th scope="col" class="sort" data-sort="name">Nombre</th>
                                    <th scope="col" class="sort" data-sort="budget">Correo</th>
                                    <th scope="col" class="sort" data-sort="status">Telefono</th>
                                    <th scope="col" class="sort" data-sort="status">DPI</th> 
                                    <th scope="col" class="sort" data-sort="status">Fecha Nacimiento</th>
                                    <th scope="col" class="sort" data-sort="status">Direccion</th>
                                    <th scope="col" class="sort" data-sort="status">Editar</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                @foreach ($pacientes ?? '' as $p)
                                    <tr>
                                        <td> {{ $p->id }} </td>
                                        <td> {{ $p->nombre }} </td>
                                        <td> {{ $p->correo }} </td>
                                        <td> {{ $p->telefono }} </td>
                                        <td> {{ $p->dpi }} </td> 
                                        <td> {{ $p->fecha_nacimiento }} </td>
                                        <td> {{ $p->direccion }} </td>
                                        <td> 
                                            <a href="/paciente/{{$p->id}}/edit" class="mx-1">
                                                <i class="fas fa-edit" title="Editar paciente"></i>
                                            </a>

                                            <a href="/paciente/{{$p->id}}" class="mx-1">
                                                <i class="fas fa-eye" title="Editar paciente"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>

                    </div>
                    <!-- Card footer -->
                    <div class="card-footer py-4 paginacion"> 
                        {{ $pacientes->appends(['buscar' => $buscar ])->links() }}
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
