@extends('layouts.app', ['title' => __('User Profile')])

@section('content')
@include('layouts.headers.empty')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-3"></div>
            <div class="col-xl-6 ">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="mb-0">{{ __('Registrar Paciente') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('paciente.store') }}" autocomplete="off">
                            @csrf
                            <div class="pl-lg-4">

                                <div class="form-group{{ $errors->has('dpi') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('DPI - formato (NNNN NNNNN NNNN)') }}</label>
                                    <input type="text" v-model="dpi" v-on:keyup="formatOnlyNumbers()" name="dpi" class="numeric form-control" placeholder="DPI" required >
                                </div> 
                                
                                <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Nombre') }}</label>
                                    <input type="text" name="nombre"  class="form-control" placeholder="nombre" required >
                                </div>
                                <div class="form-group{{ $errors->has('departamento_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Municipio') }}</label>
                                    {{ Form::select('departamento_id', $departamentos, 16, ['class' => 'form-control']) }}
                                </div> 


                                <div class="form-group{{ $errors->has('municipio_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Municipio') }}</label>
                                    {{ Form::select('municipio_id', $municipios, 1, ['class' => 'form-control']) }}
                                </div> 
                                

                                <div class="form-group{{ $errors->has('genero_id') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Genero') }}</label>
                                    <select name="genero_id" id="" class="form-control">
                                        <option>seleccionar</option>
                                          @foreach ($generos as $g)
                                             <option value="{{$g->id}}">
                                               {{$g->nombre}}
                                            </option>
                                           @endforeach
                                    </select>
                                </div>


                                <div class="form-group{{ $errors->has('fecha_nacimiento') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Fecha Nacimiento') }}</label>
                                    <input type="date" name="fecha_nacimiento" class="form-control" placeholder="fecha nacimiento" required >
                                </div>
                                <div class="form-group{{ $errors->has('correo') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Correo') }}</label>
                                    <input type="text" name="correo" class="form-control" placeholder="correo" >
                                </div>
                                <div class="form-group{{ $errors->has('telefono') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Telefono') }}</label>
                                    <input type="text" name="telefono"  class="form-control" placeholder="telefono" >
                                </div> 
                                <div class="form-group{{ $errors->has('direccion') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Direccion') }}</label>
                                    <input type="text" name="direccion" class="form-control" placeholder="direccion" required >
                                </div>
                                <div class="text-center">
                                    <button type="submit" v-bind:disabled="habilitarEnvio(dpi)" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xl-3"></div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection
@push('js')
   <script> 
         $('select[name=departamento_id]').change(function() {
            fetch("/api/municipios/" + $(this).val())
                .then(data => data.json())
                .then(json => {
                    let options = "";
                    json.forEach(municipio => {
                        options += '<option value="' + municipio.id + '">' + municipio.nombre + '</option>';
                    });
                    $('select[name=municipio_id]').html(options);
                })
                .catch(err => console.log(err))
        })

   </script>
@endpush
