@extends('layouts.app')


@push('css')
<style>
    .paginacion  svg {
        width: 25px;
    }
    
    .paginacion > nav > div > a {
        display: none;
    } 
    
    .paginacion > nav > div > span {
        display: none;
    }
</style>
@endpush
@section('content')
    @include('layouts.headers.empty')

    <div class="container-fluid mt--8">
        <div class="row">
            <div class="col ">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                          <div class="col-8">
                            <h3 class="mb-0">Listado de medicos</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="/tipo-estudio/create" class="btn btn-sm btn-primary">Agregar Tipo Estudio</a>
                        </div>
                        </div>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" class="sort" data-sort="name">Id</th>
                                    <th scope="col" class="sort" data-sort="budget">Nombre</th>
                                    <th scope="col" class="sort" data-sort="status">Fecha Creacion</th>
                                    <th scope="col" class="sort" data-sort="status">Editar</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                @foreach ($estudios ?? '' as $t)
                                    <tr>
                                        <td>
                                            {{ $t->id }}
                                        </td>
                                        <td>
                                            {{ $t->nombre }}
                                        </td>
                                        <td>
                                            {{ $t->created_at }}
                                        </td>
                                        <td>
                                            <a href="/tipo-estudio/{{$t->id}}/edit">
                                                <i class="fas fa-edit" title="Editar"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>

                    </div>
                    <!-- Card footer -->
                    <div class="card-footer py-4 paginacion"> 
                        {{ $estudios->links() }} 
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
