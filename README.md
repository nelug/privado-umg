# Privado UMG

## Comandos para Infrastructura
* Para generar las imagenes a utilizar
```bash
docker-compose build app
```
* Para montar los conetenedores e iniciarlos
```bash
docker-compose up -d
```
* Para detener los contenedores
```bash
docker-compose stop
```
* Para eliminar los contenedores
```bash
docker-compose down
```

## Comandos para trabajar
* Para instalar dependencias con composer

    * Linux
        ```bash
        docker-compose exec app composer install
        ```
    * Windows
        ```bash
        winpty docker-compose exec app composer install
        ```
* Para generar migraciones

    * Linux
        ```bash
        docker-compose exec app php artisan migrate
        ```
    * Windows
        ```bash
        winpty docker-compose exec app php artisan migrate
        ```
* Para ingresar datos a las tablas

    * Linux
        ```bash
        docker-compose exec app php artisan db:seed
        ```
    * Windows
        ```bash
        winpty docker-compose exec app php artisan db:seed
        ```
* Para para generar migraciones e ingresar datos

    * Linux
        ```bash
        docker-compose exec app php artisan migrate --seed
        ```
    * Windows
        ```bash
        winpty docker-compose exec app php artisan migrate --seed
        ```
* Para para realizar un rollback de las migraciones

    * Linux
        ```bash
        docker-compose exec app php artisan migrate:rollback
        ```
    * Windows
        ```bash
        winpty docker-compose exec app php artisan migrate:rollback
        ```

## Puertos de los contenedores


* app - laravel 7.x
```
    http://localhost:9020
```

* bd - postgresql
```
    http://localhost:5557
```

* user - test
```
    user: admin@admin.com
    pass: admin
```