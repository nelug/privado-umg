<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']); 
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);

	//Route::resource('ingreso'     , App\Http\Controllers\IngresoController::class);
	Route::resource('paciente'    , App\Http\Controllers\PacienteController::class);
	Route::resource('medico'      , App\Http\Controllers\MedicoController::class);
	Route::resource('tipo-estudio', App\Http\Controllers\TipoEstudioController::class);

	Route::resource('paciente/{id}/ingreso'     , App\Http\Controllers\IngresoController::class);
	Route::post('paciente/{id}/ingreso/{ingreso}/upload'     ,[App\Http\Controllers\IngresoController::class, 'update']);
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
